{
  description = "A customized neovim configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    # Neovim plugins (outside nixpkgs)
    cokeline = {
      url = "github:noib3/nvim-cokeline";
      flake = false;
    };
    sonokai = {
      url = "github:sainnhe/sonokai";
      flake = false;
    };
    nvim-neo-tree = {
      url = "github:nvim-neo-tree/neo-tree.nvim?ref=main";
      #url = "github:nvim-neo-tree/neo-tree.nvim?ref=v2.x";
      flake = false;
    };
  };

  outputs =
    inputs@{ nixpkgs, ... }:
    let
      utils = import ./utils.nix;
      pluginOverlay = import ./pluginOverlay.nix { inherit inputs; };
    in
    utils.outputsForEachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ pluginOverlay ];
        };
        nvim-ai = pkgs.callPackage ./default.nix { };
      in
      {
        packages.default = nvim-ai;
        packages.nvim-ai = nvim-ai;
      }
    );
}
