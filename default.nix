{
  lib,
  callPackage,
  neovim-unwrapped,
  wrapNeovimUnstable,
  neovimUtils,
  vimPlugins,
  vimUtils,
}:

let
  modules = callPackage ./modules { };
  extraMakeWrapperArgs = lib.optionalString (
    modules.packages != [ ]
  ) ''--suffix PATH : "${lib.makeBinPath modules.packages}"'';
  config = neovimUtils.makeNeovimConfig {
    vimAlias = true;

    plugins = [
      # Load runtime config first to expose utils used in other plugin configs
      {
        plugin = vimUtils.buildVimPlugin {
          name = "runtime_config";
          src = ./runtime_config;
          dependencies = with vimPlugins; [
            which-key-nvim
            nvim-neo-tree
            nvim-lspconfig
          ];
        };
        config = ''
          lua <<EOL
            require('config.options')
            require('config.keybinds')
          EOL
        '';
      }
    ] ++ modules.plugins;

    customRC = modules.config;
  };
in
wrapNeovimUnstable neovim-unwrapped (
  config // { wrapperArgs = (lib.escapeShellArgs config.wrapperArgs) + " " + extraMakeWrapperArgs; }
)
