{ vimPlugins }:
{
  plugins = [
    {
      plugin = vimPlugins.which-key-nvim;
      type = "lua";
      # See https://github.com/folke/which-key.nvim#%EF%B8%8F-configuration
      config = ''
        lua << EOL
        require('which-key').setup {
          win = {
            border = "double",
          },
        }
        EOL
      '';
    }
  ];
}
