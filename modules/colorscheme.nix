{ vimPlugins }:
{
  plugins = [
    { plugin = vimPlugins.sonokai; }
    { plugin = vimPlugins.tokyonight-nvim; }
  ];
  config = ''
    colorscheme tokyonight-night
    set termguicolors
    syntax enable
  '';
}
