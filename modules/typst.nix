{ vimPlugins, websocat }:
{
  plugins = [
    {
      plugin = vimPlugins.typst-preview-nvim;
      config = ''
        lua <<EOL
        require('typst-preview').setup({

          -- Dark mode if enabled in browser (default is never)
          --invert_colors = 'auto',

          dependencies_bin = {
            ['tinymist'] = 'tinymist',
            ['websocat'] = '${websocat}/bin/websocat',
          },
        })
        EOL
      '';
    }
  ];

  # Dependencies
  # Note: tinymist is also needed, let the project supply the correct version
  packages = [
    websocat
  ];
}
