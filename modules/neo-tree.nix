{ pkgs, vimPlugins }:
{
  plugins = [
    # Dependencies
    pkgs.vimPlugins.nvim-web-devicons
    pkgs.vimPlugins.plenary-nvim
    pkgs.vimPlugins.nui-nvim

    {
      plugin = vimPlugins.nvim-neo-tree;
      config = ''
        lua require('config.neo-tree')
      '';
    }
  ];
}
