{ pkgs, ... }:
{
  plugins = [
    pkgs.vimPlugins.telescope-fzf-native-nvim

    {
      plugin = pkgs.vimPlugins.telescope-nvim;
      config = ''
        lua <<EOL
        do
          require('telescope').load_extension('fzf')

          require('which-key').add({
            { '<leader>f', group = "Telescope", },
            { '<leader>ff', "<cmd>Telescope find_files<cr>",
              desc = "Find files",
            },
            { '<leader>fg', "<cmd>Telescope live_grep<cr>",
              desc = "Search in files",
            },
            { '<leader>fb', "<cmd>Telescope buffers<cr>",
              desc = "Find buffers",
            },
            { '<leader>fh', "<cmd>Telescope help_tags<cr>",
              desc = "Find help tags",
            },
          })
        end
        EOL
      '';
    }
  ];

  packages = [
    pkgs.fd
    pkgs.ripgrep
  ];
}
