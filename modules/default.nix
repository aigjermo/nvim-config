{
  callPackage,
  vimPlugins,
}:
let
  flatten = x: if builtins.isList x then builtins.concatMap (y: flatten y) x else [ x ];
  plugins = list: flatten (map (x: x.plugins) (builtins.filter (x: x ? plugins) list));
  packages = list: flatten (map (x: x.packages) (builtins.filter (x: x ? packages) list));
  config =
    list: builtins.concatStringsSep "\n" (map (x: x.config) (builtins.filter (x: x ? config) list));

  modules = [
    (callPackage ./bars.nix { })
    (callPackage ./colorscheme.nix { })
    (callPackage ./focus-mode.nix { })
    (callPackage ./lsp.nix { })
    (callPackage ./neo-tree.nix { })
    (callPackage ./tagbar.nix { })
    (callPackage ./telescope.nix { })
    (callPackage ./tree-sitter.nix { })
    (callPackage ./typst.nix { })
    (callPackage ./which-key.nix { })
  ];
in
{
  packages = packages modules;

  plugins = plugins modules ++ [
    vimPlugins.vim-fugitive
    vimPlugins.editorconfig-vim
  ];

  # Config is appended after plugin configs
  config = config modules;
}
