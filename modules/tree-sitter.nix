{ pkgs, ... }:
{
  packages = [
    pkgs.gcc
    pkgs.nodejs
    pkgs.tree-sitter
  ];

  plugins = [
    {
      plugin = pkgs.vimPlugins.nvim-treesitter.withPlugins (p: [
        p.bash
        p.c
        p.cpp
        p.css
        p.dockerfile
        p.go
        p.html
        p.javascript
        p.jq
        p.json
        p.lua
        p.make
        p.markdown
        p.nix
        p.proto
        p.regex
        p.rust
        p.typescript
        p.vim
        p.vimdoc
        p.yaml
      ]);
      config = ''
        lua <<EOL
        require'nvim-treesitter.configs'.setup {
          highlight = {
            -- `false` will disable the whole extension
            enable = true,

            -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
            -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
            -- the name of the parser)
            -- list of language that will be disabled
            --disable = { "c", "rust" },

            -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
            -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
            -- Using this option may slow down your editor, and you may see some duplicate highlights.
            -- Instead of true it can also be a list of languages
            additional_vim_regex_highlighting = false,
          },
          incremental_selection = {
            enable = true,
            keymaps = {
              init_selection = "gnn",
              node_incremental = "grn",
              scope_incremental = "grc",
              node_decremental = "grm",
            },
          },
          indent = {
            -- NOTE: Experimental
            -- Indentation from treesitter based on `=`
            enable = true
          },
        }
        EOL
      '';
    }
  ];
}
