{ vimPlugins }:
{
  plugins = [
    # Dependencies of lualine
    vimPlugins.nvim-web-devicons

    {
      plugin = vimPlugins.cokeline;
      type = "lua";
      config = ''
        lua <<EOL
        do
          require('cokeline').setup()

          require('which-key').add({
            { silent = true,
              { '<S-Tab>', '<Plug>(cokeline-focus-prev)',
                desc = "Previous tab",
              },
              { '<Tab>', '<Plug>(cokeline-focus-next)',
                desc = "Next tab",
              },
              { '<C-S-Tab>', '<Plug>(cokeline-switch-prev)',
                desc = "Shift tab left",
              },
              { '<C-Tab>', '<Plug>(cokeline-switch-next)',
                desc = "Shift tab right",
              },
            },
          })
        end
        EOL
      '';
    }

    {
      plugin = vimPlugins.lualine-nvim;
      type = "lua";
      config = ''
        lua <<EOL
        require('lualine').setup {
          options = {
            icons_enabled = true,
            theme = 'auto',
            component_separators = { left = '', right = ''},
            section_separators = { left = '', right = ''},
            section_separators = ''',
            component_separators = ''',
            disabled_filetypes = {},
            always_divide_middle = true,
          },
          sections = {
            lualine_a = {'mode'},
            lualine_b = {'branch', 'diff', 'diagnostics'},
            lualine_c = {'filename'},
            lualine_x = {'encoding', 'fileformat', 'filetype'},
            lualine_y = {'progress'},
            lualine_z = {'location'}
          },
          inactive_sections = {
            lualine_a = {},
            lualine_b = {},
            lualine_c = {'filename'},
            lualine_x = {'location'},
            lualine_y = {},
            lualine_z = {}
          },
          tabline = {},
          extensions = {},
        }
        EOL
      '';
    }

    vimPlugins.lualine-lsp-progress
  ];
}
