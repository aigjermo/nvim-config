{ pkgs, vimPlugins }:
{
  plugins = [
    {
      plugin = vimPlugins.nvim-lspconfig;
      type = "lua";
      config = ''
        lua require('config.lspconfig')
      '';
    }
    {
      plugin = vimPlugins.clangd_extensions-nvim;
      type = "lua";
    }
  ];

  packages = [
    pkgs.pyright
    pkgs.nixd
    pkgs.nixfmt-rfc-style
    pkgs.rust-analyzer
    pkgs.lua-language-server
    pkgs.clang-tools_16
  ];
}
