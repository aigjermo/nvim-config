{ vimPlugins }:
{
  plugins = [
    {
      plugin = vimPlugins.goyo-vim;
      optional = true;
      config = ''
        lua <<EOL
        do -- Goyo + Limelight
          require('which-key').add({
            { '<leader>g', '<cmd>packadd goyo.vim | Goyo<cr>',
              desc = "Focus mode",
            },
          })
        end
        EOL
      '';
    }
    {
      plugin = vimPlugins.limelight-vim;
      optional = true;
      config = ''
        augroup GoyoLimeLight
          autocmd!
          autocmd User GoyoEnter packadd limelight.vim
          autocmd User GoyoEnter Limelight
          autocmd User GoyoLeave Limelight!
        augroup END
      '';
    }
  ];
}
