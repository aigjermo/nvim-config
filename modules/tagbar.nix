{ pkgs, vimPlugins }:
{
  plugins = [
    {
      plugin = vimPlugins.tagbar;
      optional = true;
      config = ''
        nmap <F3> :packadd tagbar \| TagbarToggle<CR>
        let g:tagbar_ctags_bin = "${pkgs.universal-ctags}/bin/ctags"
      '';
    }
  ];
}
