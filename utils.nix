rec {
  systems = {
    x86_64-linux = "x86_64-linux";
    aarch64-linux = "aarch64-linux";
    x86_64-darwin = "x86_64-darwin";
    aarch64-darwin = "aarch64-darwin";
  };

  defaultSystems = builtins.attrValues systems;

  insertSystemToOutputs =
    system: outputs: (builtins.mapAttrs (name: value: { ${system} = value; }) outputs);

  outputsForEachSystem =
    systems: f:
    (builtins.zipAttrsWith (name: values: builtins.foldl' (a: b: a // b) { } values) (
      builtins.map (system: insertSystemToOutputs system (f system)) systems
    ));

  outputGroupForEachSystem =
    systems: f: (builtins.foldl' (result: system: result // { ${system} = f system; }) { } systems);

  outputsForEachDefaultSystem = outputsForEachSystem defaultSystems;
  outputGroupForEachDefaultSystem = outputGroupForEachSystem defaultSystems;
}
