local wk = require('which-key')

wk.add({

  -- Quicker nav in quickfix list
  { silent = true, expr = true,
    { ']q', '"<cmd>" . v:count1 . "cnext<cr>"',
      desc = "Next from quickfix list",
    },
    { '[q', '"<cmd>" . v:count1 . "cprevious<cr>"',
      desc = "Previous from quickfix list",
    },
  },

  -- Clear highlight
  { silent = true,
    { '<leader><space>', '<cmd>noh<cr>',
      desc = "Clear higlight",
    },
  },

  -- Map escape in terminal
  { mode = 't',
    { '<C-Esc>', '<C-\\><C-n>',
      desc = "Return to terminal normal mode",
    },
    { '<C-w><Esc>', '<C-\\><C-n>',
      desc = "Return to terminal normal mode",
    }
  },
})
