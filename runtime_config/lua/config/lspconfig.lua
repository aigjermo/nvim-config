--
-- Config for `modules/lsp.nix`
--

local wk = require('which-key')

-- Lsp-config Suggested mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
wk.add({
  { name = "diagnostic",
    { '<space>q', vim.diagnostic.setqflist,
      desc = "Problem list",
    },
    { '[d', vim.diagnostic.goto_prev,
      desc = "Previous problem",
    },
    { ']d', vim.diagnostic.goto_next,
      desc = "Next problem",
    },
    { '<space>e', vim.diagnostic.open_float,
      desc = "Expose problem description",
    },
  }
})

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local map_keys_on_attach = function(_, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  wk.add({
    { buffer = bufnr,
      { 'K', vim.lsp.buf.hover, desc = "Peek documentation", },
      {'<C-k>', vim.lsp.buf.signature_help, desc = "Signature help", },

      { 'g', group = "Go to", },
      { 'gD', vim.lsp.buf.declaration, desc = "declaration", },
      { 'gd', vim.lsp.buf.definition, desc = "definition", },
      { 'gi', vim.lsp.buf.implementation, desc = "implementation", },
      { 'gr', vim.lsp.buf.References, desc = "references", },
      { 'gt', vim.lsp.buf.type_definition, desc = "type definition", },

      { '<space>', group = "Code actions", },
      { '<space>f', function() vim.lsp.buf.format { async = true } end,
        desc = "Format file",
      },

      { '<space>r', group = "Refactor", },
      { '<space>rn', vim.lsp.buf.rename, desc = "Rename symbol", },
      { '<space>rc', group = "Code suggestions", },
      { '<space>rca', vim.lsp.buf.code_action, desc = "Apply code action", },

      { '<space>w', group = "Workspace", },
      { '<space>wa', vim.lsp.buf.add_workspace_folder,
        desc = "Add workspace folder",
      },
      { '<space>wr', vim.lsp.buf.remove_workspace_folder,
        desc = "Remove workspace folder",
      },
      { '<space>wl',
        function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end,
        desc = "List workspace folders",
      },
    },
  })
end

-- Diagnostic virtual text symbold
vim.diagnostic.config({
  virtual_text = {
    prefix = '●', -- Could be '■', '▎', 'x'
  }
})

-- Symbols in gutter
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

-- Symbols for autocomplete
vim.lsp.protocol.CompletionItemKind = {
  "   (Text) ",
  "   (Method)",
  "   (Function)",
  "   (Constructor)",
  " ﴲ  (Field)",
  "[] (Variable)",
  "   (Class)",
  " ﰮ  (Interface)",
  "   (Module)",
  " 襁 (Property)",
  "   (Unit)",
  "   (Value)",
  " 練 (Enum)",
  "   (Keyword)",
  "   (Snippet)",
  "   (Color)",
  "   (File)",
  "   (Reference)",
  "   (Folder)",
  "   (EnumMember)",
  " ﲀ  (Constant)",
  " ﳤ  (Struct)",
  "   (Event)",
  "   (Operator)",
  "   (TypeParameter)",
}

-- Language servers --

local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}

require('lspconfig').clangd.setup {
  on_attach = map_keys_on_attach,
  flags = lsp_flags,
}

require('lspconfig').pyright.setup {
  on_attach = map_keys_on_attach,
  flags = lsp_flags,
}

require('lspconfig').nixd.setup {
  on_attach = map_keys_on_attach,
  flags = lsp_flags,
  settings = {
    nixd = {
      formatting = {
        command = { "nixfmt" },
      },
    },
  },
}

require('lspconfig').ruff.setup {
  init_options = {
    settings = {
      -- see: https://docs.astral.sh/ruff/editors/settings/#top-level
    }
  }
}

require('lspconfig').rust_analyzer.setup {
  on_attach = map_keys_on_attach,
  flags = lsp_flags,
}

require('lspconfig').lua_ls.setup {
  on_attach = map_keys_on_attach,
  flags = lsp_flags,
  -- Server-specific settings...
  settings = {
    Lua = {
      diagnostics = {
        globals = { 'vim' },
      },
    },
    workspace = {
      -- Make the server aware of Neovim runtime files
      library = vim.api.nvim_get_runtime_file("", true),
    },
    -- Do not send telemetry data containing a randomized but unique identifier
    telemetry = {
      enable = false,
    },
  }
}

require('lspconfig').tinymist.setup {
  on_attach = map_keys_on_attach,
  flags = lsp_flags,

  -- Workaround for nvim issue #30675 (will be fixed in 0.10.3)
  offset_encoding = 'utf-8',

  -- Server-specific settings...
  settings = {
    formatterMode = "typstyle",
  }
}
