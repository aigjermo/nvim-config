do -- Neo-tree
  local wk = require('which-key')

  -- Remove the deprecated commands from v1.x
  vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])

  require('neo-tree').setup({
    window = {
      mapping_options = {
        noremap = true,
        nowait = true,
      },
      mappings = {
        ["<space>"] = {
          "toggle_node",
          -- disable nowait to enable existing combos starting with space
          nowait = false,
        },
        ["<2-LeftMouse>"] = "open",
        ["<cr>"] = "open",
        ["S"] = "open_split",
        ["s"] = "open_vsplit",
        ["t"] = "open_tabnew",
        ["w"] = "open_with_window_picker",
        ["C"] = "close_node",
        ["a"] = {
          "add",
          config = {
            show_path = "none" -- "none", "relative", "absolute"
          },
        },
        ["A"] = {
          "add_directory",
          config = {
            show_path = "none" -- "none", "relative", "absolute"
          },
        },
        ["d"] = "delete",
        ["r"] = "rename",
        ["y"] = "copy_to_clipboard",
        ["x"] = "cut_to_clipboard",
        ["p"] = "paste_from_clipboard",
        ["c"] = {
          "copy",              -- takes text input for destination
          config = {
            show_path = "none" -- "none", "relative", "absolute"
          },
        },
        ["m"] = {
          "move",              -- takes text input for destination
          config = {
            show_path = "none" -- "none", "relative", "absolute"
          },
        },
        ["q"] = "close_window",
        ["R"] = "refresh",
        ["?"] = "show_help",
      },
    },
    filesystem = {
      window = {
        position = "current"
      },
    },
    buffers = {
      window = {
        position = "current"
      },
      show_unloaded = true,
    },
    git_status = {
      window = {
        position = "float",
      },
      mappings = {
        ["ga"] = "git_add_file",
        ["gA"] = "git_add_all",
        ["gu"] = "git_unstage_file",
        ["gr"] = "git_revert_file",
        ["gc"] = "git_commit",
      },
    },
  })

  wk.add({
    { '<f2>', "<cmd>Neotree filesystem reveal<cr>", desc = "Open Neo-tree", },

    { '<leader>t', group = "Neo-tree", },
    { '<leader>tf', "<cmd>Neotree filesystem reveal<cr>", desc = "Files", },
    { '<leader>tg', "<cmd>Neotree git_status<cr>", desc = "Git status", },
    { '<leader>tb', "<cmd>Neotree buffers<cr>", desc = "Buffers", },
  })
end
