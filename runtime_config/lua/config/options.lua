--== Basic options ==--

-- Encoding
vim.o.encoding = "utf-8"

-- Kill nosiy bell
vim.o.visualbell = true

-- Enable mouse stuff
vim.o.mouse = 'a'

-- Text wrapping
vim.o.wrap = true         -- Wrap long lines of text...
vim.o.textwidth = 80      -- at 80 characters
vim.o.linebreak = true    -- Don't break in the middle of words
vim.o.colorcolumn = "+0"  -- Draw a line to mark the boundary

vim.o.formatoptions = "ctjroql"    -- Flags controlling automatic formatting:
      -- c :  Autowrap comments and add comment leaders on subsequent lines.
      -- t :  Auto-wrap text using textwidth
      -- j :  Remove leaders when joining comment lines.
      -- r :  Add comment leader after hitting enter in Insert mode.
      -- o :  Add comment leader after hitting 'o' or 'O' in Normal mode.
      -- q :  Allow formatting comments with 'gq'.
      -- l :  Long lines are not broken in insert mode: When a line was longer
      --      than 'textwidth' when the insert command started, Vim does not
      --      automatically format it.

-- Indentation & tabs vs spaces (defaults)
vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true
vim.o.smartindent = true

-- Line numbers
vim.o.number = true
vim.o.relativenumber = true

-- Searching
vim.o.hlsearch = true     -- Highlight all search matches on start
vim.o.incsearch = true    -- Highlight matches as you type
vim.o.ignorecase = true   -- Ignore casing in search by default
vim.o.smartcase = true    -- Override to case sensitive if uppercase is found

-- Backspace behavior
vim.o.backspace = "indent,eol,start"  -- Allow backspacing into areas beyond:
                      -- * start, where cursor was when entering insert mode
                      -- * indent, autoindented spaces after adding a line
                      -- * eol, Past a line break (deleting a line)

-- Visibility of hidden stuff
vim.opt.listchars = {
  nbsp = '¬',       -- Non breaking space
  tab = '▸-',       -- Tabs
  trail = '·',      -- Trailing whitespace
  extends = '»',
  precedes = '«'
}
vim.opt.list = true

-- Avoid extra signcolumn
vim.o.signcolumn = 'number'     -- Mixes signs into number column

-- Leader key
vim.g.mapleader = ','
