{ inputs }:
prev: final: {
  vimPlugins = final.vimPlugins // {
    sonokai = (
      prev.vimUtils.buildVimPlugin {
        name = "sonokai";
        src = inputs.sonokai;
      }
    );
    cokeline = (
      prev.vimUtils.buildVimPlugin {
        name = "cokeline";
        src = inputs.cokeline;
        dependencies = [ final.vimPlugins.plenary-nvim ];
      }
    );
    nvim-neo-tree = (
      prev.vimUtils.buildVimPlugin {
        name = "neo-tree";
        src = inputs.nvim-neo-tree;
        doCheck = false;
        buildPhase = ''
          echo "Overriding build phase to avoid running make (and failing)"
        '';
      }
    );
  };
}
